package messagebroker

import (
	"fmt"
	"os"

	"github.com/spf13/cast"
)

type MESSAGE_BROKER_TYPE string

var (
	VALID_CLIENT_TYPE = map[string]bool{
		"rabbitmq": true,
		"kafka":    true,
		"nsq":      true,
		"redis":    true,
	}

	MESSAGE_BROKER_TYPE_RABBITMQ    MESSAGE_BROKER_TYPE = MESSAGE_BROKER_TYPE("rabbitmq")
	MESSAGE_BROKER_TYPE_KAFKA       MESSAGE_BROKER_TYPE = MESSAGE_BROKER_TYPE("kafka")
	MESSAGE_BROKER_TYPE_NSQ         MESSAGE_BROKER_TYPE = MESSAGE_BROKER_TYPE("nsq")
	MESSAGE_BROKER_TYPE_REDISPUBSUB MESSAGE_BROKER_TYPE = MESSAGE_BROKER_TYPE("redis")
)

func LoadClientConfigFromEnv(prefix string) (cfg ClientConfig, err error) {
	clientTypeStr := os.Getenv(fmt.Sprintf("%s_CLIENT_TYPE", prefix))
	isValidClientType := VALID_CLIENT_TYPE[clientTypeStr]
	if !isValidClientType {
		err = fmt.Errorf("invalid client type")
		return
	}

	cfg = ClientConfig{
		Type:     MESSAGE_BROKER_TYPE(clientTypeStr),
		Host:     os.Getenv(fmt.Sprintf("%s_HOST", prefix)),
		Port:     cast.ToInt(os.Getenv(fmt.Sprintf("%s_PORT", prefix))),
		Username: os.Getenv(fmt.Sprintf("%s_USERNAME", prefix)),
		Password: os.Getenv(fmt.Sprintf("%s_PASSWORD", prefix)),
	}

	return
}

type ClientConfig struct {
	Type     MESSAGE_BROKER_TYPE
	Host     string
	Port     int
	Username string
	Password string
}

type ConsumerConfig struct {
	Type         MESSAGE_BROKER_TYPE
	Topic        string
	ExchangeName string
	RouteKey     string
	QueueName    string
	ConsumerName string
	ExchangeType string
}

type PublisherConfig struct {
	Type         MESSAGE_BROKER_TYPE
	Topic        string
	QueueName    string
	ExchangeName string
	ExchangeType string
	Timeout      int64
	Message      []byte
	RouteKey     string
}

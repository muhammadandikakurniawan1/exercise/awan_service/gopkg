package rabbitmq

import (
	"context"
	"fmt"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/messagebroker"

	"github.com/avast/retry-go"
	"github.com/streadway/amqp"
)

func NewClient(config messagebroker.ClientConfig) (client *Client, err error) {
	url := fmt.Sprintf("amqp://%s:%s@%s:%d/", config.Username, config.Password, config.Host, config.Port)
	client = &Client{}
	var conn *amqp.Connection
	err = retry.Do(func() (err error) {
		conn, err = amqp.Dial(url)
		return
	}, retry.Attempts(3), retry.LastErrorOnly(true))

	if err != nil {
		panic(err)
	}

	errC := conn.NotifyClose(make(chan *amqp.Error, 10))
	go func(cln *Client) {
		for err := range errC {
			if err != nil {
				c, _ := NewClient(config)
				client.conn = c.conn
				client.channelConn = c.channelConn
			}
		}
	}(client)

	channelConn, err := conn.Channel()
	if err != nil {
		return
	}

	client.conn = conn
	client.channelConn = channelConn

	errChan := conn.NotifyClose(make(chan *amqp.Error, 10))
	go func(cln *Client) {
		for err := range errChan {
			if err != nil {
				c, _ := NewClient(config)
				cln.conn = c.conn
				cln.channelConn = c.channelConn
			}
		}
	}(client)

	return
}

type Client struct {
	conn        *amqp.Connection
	channelConn *amqp.Channel
}

func (c Client) Consume(ctx context.Context, config messagebroker.ConsumerConfig, handler messagebroker.ConsumerHandler, onError func(handlerErr error)) (err error) {

	ch, err := c.conn.Channel()
	if err != nil {
		return
	}
	defer ch.Close()

	queue, err := ch.QueueDeclare(
		config.QueueName, // name
		false,            // durable
		false,            // delete when unused
		false,            // exclusive
		false,            // no-wait
		nil,              // arguments
	)
	if err != nil {
		return
	}

	msgs, err := ch.Consume(
		queue.Name,          // queue
		config.ConsumerName, // consumer
		true,                // auto-ack
		false,               // exclusive
		false,               // no-local
		false,               // no-wait
		nil,                 // args
	)
	if err != nil {
		return
	}

	forever := make(chan bool)
	go func() {
		for msg := range msgs {
			handlerErr := handler(ctx, msg.Body, forever)
			if handlerErr != nil {
				onError(handlerErr)
			}
		}
	}()
	<-forever
	return
}

func (c Client) Publish(ctx context.Context, config messagebroker.PublisherConfig) (err error) {
	ch, err := c.conn.Channel()
	if err != nil {
		return
	}
	defer ch.Close()

	// We create a Queue to send the message to.
	q, err := ch.QueueDeclare(
		config.QueueName, // name
		false,            // durable
		false,            // delete when unused
		false,            // exclusive
		false,            // no-wait
		nil,              // arguments
	)
	if err != nil {
		return
	}

	err = ch.Publish(
		"",     // exchange
		q.Name, // routing key
		false,  // mandatory
		false,  // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        config.Message,
		})

	return
}

func (c Client) PublishExchangeStreamData(ctx context.Context, configCh <-chan messagebroker.PublisherConfig) (err error) {
	ch, err := c.conn.Channel()
	if err != nil {
		return
	}
	// defer ch.Close()

	exchangeAlreadyDeclare := false

	for config := range configCh {
		if !exchangeAlreadyDeclare {
			err = ch.ExchangeDeclare(
				config.ExchangeName, // name
				config.ExchangeType, // kind
				false,               // durable
				false,               // auto-deleted
				false,               // internal
				false,               // no-wait
				nil,                 // arguments
			)
			if err != nil {
				return
			}
			exchangeAlreadyDeclare = true
		}

		err = ch.Publish(
			config.ExchangeName, // exchange
			config.RouteKey,     // routing key
			false,               // mandatory
			false,               // immediate
			amqp.Publishing{
				ContentType: "text/plain",
				Body:        config.Message,
			},
		)
		if err != nil {
			return
		}
	}

	return
}

func (c Client) PublishExcange(ctx context.Context, config messagebroker.PublisherConfig) (err error) {
	// ch, err := c.conn.Channel()
	// if err != nil {
	// 	return
	// }
	// defer ch.Close()

	err = c.channelConn.ExchangeDeclare(
		config.ExchangeName, // name
		config.ExchangeType, // kind
		false,               // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		return
	}

	// ctx, cancel := context.WithTimeout(context.Background(), time.Duration(config.Timeout)*time.Second)
	// defer cancel()

	err = c.channelConn.Publish(
		config.ExchangeName, // exchange
		config.RouteKey,     // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        config.Message,
		})

	return
}

func (c Client) ConsumeExchange(ctx context.Context, config messagebroker.ConsumerConfig, handler messagebroker.ConsumerHandler, onError func(handlerErr error, chConsumer chan<- bool)) (err error) {

	ch, err := c.conn.Channel()
	if err != nil {
		return
	}
	defer ch.Close()

	err = ch.ExchangeDeclare(
		config.ExchangeName, // name
		config.ExchangeType, // kind
		false,               // durable
		false,               // auto-deleted
		false,               // internal
		false,               // no-wait
		nil,                 // arguments
	)
	if err != nil {
		return
	}

	queue, err := ch.QueueDeclare(
		config.QueueName, // name
		false,            // durable
		false,            // delete when unused
		true,             // exclusive
		false,            // no-wait
		nil,              // arguments
	)
	if err != nil {
		return
	}

	err = ch.QueueBind(queue.Name, config.RouteKey, config.ExchangeName, false, nil)
	if err != nil {
		return
	}

	msgs, err := ch.Consume(
		queue.Name,          // queue
		config.ConsumerName, // consumer
		true,                // auto-ack
		false,               // exclusive
		false,               // no-local
		false,               // no-wait
		nil,                 // args
	)
	if err != nil {
		return
	}

	forever := make(chan bool)
	go func() {
		for msg := range msgs {
			handlerErr := handler(ctx, msg.Body, forever)
			if handlerErr != nil {
				onError(handlerErr, forever)
			}
		}
	}()
	<-forever
	return
}

func (c Client) Close() (err error) {
	err = c.channelConn.Close()
	if err != nil {
		return
	}
	return c.conn.Close()
}

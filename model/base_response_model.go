package model

type BaseUsecaseResponseModel[T_DATA any] struct {
	Data           T_DATA `json:"data"`
	Message        string `json:"message"`
	ErrorMessage   string `json:"error_message"`
	Success        bool   `json:"success"`
	StatusCode     string `json:"status_code"`
	HttpStatusCode int    `json:"http_status_code"`
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetData(data T_DATA) *BaseUsecaseResponseModel[T_DATA] {
	m.Data = data
	return m
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetMessage(data string) *BaseUsecaseResponseModel[T_DATA] {
	m.Message = data
	return m
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetErrorMessage(data string) *BaseUsecaseResponseModel[T_DATA] {
	m.ErrorMessage = data
	return m
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetSuccess(data bool) *BaseUsecaseResponseModel[T_DATA] {
	m.Success = data
	return m
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetStatusCode(data string) *BaseUsecaseResponseModel[T_DATA] {
	m.StatusCode = data
	return m
}

func (m *BaseUsecaseResponseModel[T_DATA]) SetHttpStatusCode(data int) *BaseUsecaseResponseModel[T_DATA] {
	m.HttpStatusCode = data
	return m
}

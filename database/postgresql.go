package database

import (
	"fmt"
	"strings"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPostGresqlDB(host string, port uint, user, pass, dbName string, config gorm.Config, params ...string) (*Database, error) {

	dsnBuilder := strings.Builder{}
	dsnBuilder.WriteString(fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s", host, port, user, pass, dbName))
	for _, p := range params {
		dsnBuilder.WriteString(fmt.Sprintf(" %s", p))
	}

	dsn := dsnBuilder.String()
	db, err := gorm.Open(postgres.Open(dsn), &config)
	if err != nil {
		return nil, err
	}
	return &Database{db}, nil
}

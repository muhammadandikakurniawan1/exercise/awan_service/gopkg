package mongo

import "go.mongodb.org/mongo-driver/mongo"

func NewBaseMongoRepository(client *mongo.Client) BaseMongoRepository {
	return BaseMongoRepository{client}
}

type BaseMongoRepository struct {
	client *mongo.Client
}

func (o BaseMongoRepository) GetConnection() *mongo.Client {
	return o.client
}

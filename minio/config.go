package minio

import (
	"os"
	"time"

	"github.com/spf13/cast"
)

func SetupMinioConfigFromEnv() MinioConfig {
	return MinioConfig{
		Timeout:   time.Duration(cast.ToFloat64(os.Getenv("MINIO_TIMEOUT"))),
		Host:      os.Getenv("MINIO_HOST"),
		KeyId:     os.Getenv("MINIO_KEY_ID"),
		AccessKey: os.Getenv("MINIO_ACESS_KEY"),
		Ssl:       cast.ToBool(os.Getenv("MINIO_SSL")),
	}
}

type MinioConfig struct {
	Timeout   time.Duration `json:"timeout"`
	Host      string        `json:"host"`
	KeyId     string        `json:"keyId"`
	AccessKey string        `json:"accessKey"`
	Ssl       bool          `json:"ssl"`
}

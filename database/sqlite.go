package database

import "database/sql"

func NewSqlite(file string) (db *sql.DB, err error) {
	return sql.Open("sqlite3", file)
}

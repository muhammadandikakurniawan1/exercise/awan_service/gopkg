PACKAGE_NAME := user_service

install:
	go get golang.org/x/tools/cmd/cover
	go install github.com/vektra/mockery/v2@latest
	go mod tidy
	go mod download

gen-proto:
	protoc --go_out=. --go-grpc_out=. proto/auth_service/auth/*.proto
	protoc --go_out=. --go-grpc_out=. proto/auth_service/user/*.proto
	protoc --go_out=. --go-grpc_out=. proto/user_service/user_group/*.proto
	protoc --go_out=. --go-grpc_out=. proto/user_service/user/*.proto
	protoc --go_out=. --go-grpc_out=. proto/user_service/auth/*.proto

test:
	@echo "=================================================================================="
	@echo "Coverage Test"
	@echo "=================================================================================="
	go fmt ./... && go test -coverprofile coverage.cov -cover ./... # use -v for verbose
	@echo "\n"
	@echo "=================================================================================="
	@echo "All Package Coverage"
	@echo "=================================================================================="
	go tool cover -func coverage.cov

mock:
	@echo "=================================================================================="
	@echo "Generating Mock"
	@echo "=================================================================================="
	@echo "Make sure install mockery https://github.com/vektra/mockery#installation"
	@echo "\n"
	mockery --all --output mocks --case underscore
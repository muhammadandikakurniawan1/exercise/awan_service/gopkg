package circuitbreaker

import (
	"github.com/sony/gobreaker"
)

type CircuitBreaker interface {
	Execute(req func() (interface{}, error)) (interface{}, error)
}

func NewCircuitBreaker(cfg Config) CircuitBreaker {

	cbSetting := gobreaker.Settings{
		Name:        cfg.Name,
		MaxRequests: cfg.MaxRequests,
		Timeout:     cfg.Timeout,
		Interval:    cfg.Interval,
	}

	if cfg.ReadyToTrip != nil {
		cbSetting.ReadyToTrip = func(counts gobreaker.Counts) bool {
			c := Counts{
				Requests:             counts.Requests,
				TotalSuccesses:       counts.TotalSuccesses,
				TotalFailures:        counts.TotalFailures,
				ConsecutiveSuccesses: counts.ConsecutiveSuccesses,
				ConsecutiveFailures:  counts.ConsecutiveFailures,
			}
			return cfg.ReadyToTrip(c)
		}
	}

	if cfg.OnStateChange != nil {
		cbSetting.OnStateChange = func(name string, from, to gobreaker.State) {
			f := ConvertState(from)
			t := ConvertState(to)
			cfg.OnStateChange(name, f, t)
		}
	}

	cb := gobreaker.NewCircuitBreaker(cbSetting)

	return &circuitBreakerImpl{
		cb: cb,
	}
}

type circuitBreakerImpl struct {
	cb *gobreaker.CircuitBreaker
}

func (cb circuitBreakerImpl) Execute(req func() (interface{}, error)) (result interface{}, err error) {
	return cb.Execute(req)
}

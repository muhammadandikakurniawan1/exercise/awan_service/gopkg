package delivery_client

type BaseResponseModel struct {
	Code         string                  `json:"code"`
	Success      bool                    `json:"success"`
	ErrorMessage interface{}             `json:"error_message"`
	Message      string                  `json:"message"`
	Pagination   ResponsePaginationModel `json:"pagination"`
}

type ResponsePaginationModel struct {
	Page      uint64 `json:"page"`
	TotalData uint64 `json:"total_data"`
	TotalPage uint64 `json:"total_page"`
	PerPage   uint64 `json:"per_page"`
}

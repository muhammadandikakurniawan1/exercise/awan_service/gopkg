package mongo

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func NewConnection(props ConnectionProperty) (client *mongo.Client, err error) {
	conStrt := fmt.Sprintf("mongodb://%s:%s@%s:%d/", props.Username, props.Password, props.Host, props.Port)

	client, err = mongo.NewClient(options.Client().ApplyURI(conStrt))
	if err != nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(ctx)
	return
}

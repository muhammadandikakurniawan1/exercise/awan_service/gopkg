package mongo

type ConnectionProperty struct {
	Host     string
	Port     uint
	Username string
	Password string
}

package primitive

type Entity interface {
	GetIdentifier() interface{}
}

type BaseEntity struct {
	entityIdentifier interface{}
}

func (o BaseEntity) GetIdentifier() interface{} {
	return o.entityIdentifier
}

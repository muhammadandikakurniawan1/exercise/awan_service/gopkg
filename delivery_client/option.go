package delivery_client

import (
	"fmt"
	"time"

	"gitlab.com/muhammadandikakurniawan1/exercise/awan_service/gopkg/utility"

	"github.com/spf13/cast"
)

type Options struct {
	AppName           string        `json:"appName"`
	Address           string        `json:"address"`
	Timeout           time.Duration `json:"timeout"`
	DebugMode         bool          `json:"debugMode"`
	WithProxy         bool          `json:"withProxy"`
	ProxyAddress      string        `json:"proxyAddress"`
	SkipTLS           bool          `json:"skipTLS"`
	SkipCheckRedirect bool          `json:"skipCheckRedirect"`
	SysLog            bool          `json:"sysLog"`
}

func LoadOptionsFromEnvironment(prefix string) Options {
	opt := Options{
		AppName:           utility.GetEnv("APP_NAME", "service"),
		Address:           utility.GetEnv(fmt.Sprintf("%s_HOST", prefix), "http://localhost:8080"),
		Timeout:           time.Duration(cast.ToInt64(utility.GetEnv(fmt.Sprintf("%s_TIMEOUT", prefix), "30"))),
		DebugMode:         cast.ToBool(utility.GetEnv(fmt.Sprintf("%s_DEBUG", prefix), "false")),
		WithProxy:         cast.ToBool(utility.GetEnv(fmt.Sprintf("%s_PROXY", prefix), "false")),
		ProxyAddress:      utility.GetEnv(fmt.Sprintf("%s_PROXY_HOST", prefix), ""),
		SkipTLS:           cast.ToBool(utility.GetEnv(fmt.Sprintf("%s_SKIP_TLS", prefix), "true")),
		SkipCheckRedirect: cast.ToBool(utility.GetEnv(fmt.Sprintf("%s_SKIP_REDIRECT", prefix), "false")),
		SysLog:            cast.ToBool(utility.GetEnv(fmt.Sprintf("%s_SYS_LOG", prefix), "true")),
	}
	return opt
}

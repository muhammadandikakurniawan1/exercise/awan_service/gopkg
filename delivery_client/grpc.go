package delivery_client

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

type RpcConnection struct {
	options Options
	Conn    *grpc.ClientConn
}

func NewGRpcConnection(options Options) *RpcConnection {
	var conn *grpc.ClientConn
	var err error
	conn, err = grpc.Dial(options.Address, grpc.WithInsecure(), withClientUnaryInterceptor())

	if err != nil {
		panic(err)
	}

	return &RpcConnection{
		Conn:    conn,
		options: options,
	}
}

func clientInterceptor(
	ctx context.Context,
	method string,
	req interface{},
	reply interface{},
	cc *grpc.ClientConn,
	invoker grpc.UnaryInvoker,
	opts ...grpc.CallOption,
) error {
	md, _ := metadata.FromOutgoingContext(ctx)
	log.Println(md)
	err := invoker(ctx, method, req, reply, cc, opts...)
	return err
}

func withClientUnaryInterceptor() grpc.DialOption {
	return grpc.WithUnaryInterceptor(clientInterceptor)
}
